const Course = require("../Models/coursesSchema");
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/

module.exports.addCourse = (request, response) => {
	let input = request.body;

	let newCourse = new Course({
		name: input.name,
		description: input.description,
		price: input.price
	});

	const userData = auth.decode(request.headers.authorization);

	Course.findOne({name: input.name})
	// saves the created object to our database
	.then(result => {
		if (userData.isAdmin) {
			if (result !== null) {
				return response.send(`The course ${input.name} already exists!`);
			} else {
				newCourse.save();
				return response.send(`You have created a new course name ${input.name}!`);
			}
		} else {
			return response.send("You do not have permission to create a course.");
		}
	})
	// course creation failed
	.catch(error => {
		return response.send(error);
	})
}

// Create a controller wherein it will retrieve all the courses (active/inactive Courses)

module.exports.allCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if (!userData.isAdmin) {
		return response.send("You don't have access to this route!");
	} else {
		Course.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}

}

// Create a controller wherein it will retrieve course that are active
module.exports.allActiveCourses = (request, response) => {
	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))
}


/*
	Mini Activity
		1. You are going to create a route wherein it can retrieve all inactive courses
		2. Make sure that the admin users only are the ones that can access this route.
*/

// Create a controller wherein it will retrieve coursees that are inactive
module.exports.allInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if (!userData.isAdmin) {
		return response.send("You do not have access to this route!");
	} else {
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}
}

// This controller will get the details of specific course
module.exports.courseDetails = (request,response) => {
	const courseId = request.params.courseId;

	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

// This controller is for updating a specific course
/*
	Business logic:
		1. We are going to edit/update the course, that is stored in the params
*/
module.exports.updateCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	const input = request.body;

	if (!userData.isAdmin) {
		return response.send("You do not have access to this route!");
	} else {
		Course.findById(courseId)
		.then(result => {
			if (result == null) {
				return response.send("CourseId is invalid, please try again!");
			} else {
				let updatedCourse = {
					name: input.name,
					description: input.description,
					price: input.price
				}

				Course.findByIdAndUpdate(courseId, updatedCourse, {new:true})
				.then(result => {
					console.log(result);
					return response.send(result);
				})
				.catch(error => response.send(error));
			}
		})
		.catch(error => response.send(error));
	}
}

// This controller is for archiving courses
module.exports.archiveCourse = (request,response) => {
	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	Course.findById(courseId)
	.then(result => {
		if (!userData.isAdmin) {
			return response.send("You do not have access to this route.")
		} else {
			if (result == null) {
				return response.send("Invalid course id. Please enter a valid course id!");
			} else {
				if (!result.isActive) {
					return response.send("This course is already archived!")
				} else {
					Course.findByIdAndUpdate(courseId, {isActive: false}, {new: true})
					.then(result => response.send(`You have successfully archived ${result.name}`))
					.catch(error => response.send(error));
				}
			}
		}
	})
	.catch(error => response.send(error));
}